<template lang="pug">
  section.equipment
    //- .equipment__tomato.equipment__tomato--first
    //-   img(src="~/static/images/tomato_right_1.png" srcset="~/static/images/tomato_right_1@2x.png" v-lazy="require('~/static/images/tomato_right_1@2x.png')")
    //- .equipment__tomato.equipment__tomato--second
    //-   img(src="~/static/images/tomato_right_2.png" srcset="~/static/images/tomato_right_2@2x.png" v-lazy="require('~/static/images/tomato_right_2@2x.png')")
    .equipment__wheat
      img(src="~/static/images/wheat_1.png" srcset="~/static/images/wheat_1@2x.png").equipment__wheat-pic.equipment__wheat-pic--1
      img(src="~/static/images/wheat_2.png" srcset="~/static/images/wheat_2@2x.png").equipment__wheat-pic.equipment__wheat-pic--2
      img(src="~/static/images/wheat_3.png" srcset="~/static/images/wheat_3@2x.png").equipment__wheat-pic.equipment__wheat-pic--3
      img(src="~/static/images/wheat_big.png" srcset="~/static/images/wheat_big@2x.png").equipment__wheat-pic.equipment__wheat-pic--big
    div.tabs
      .tabs-controls
        app-btn(
          @click.native="showEquipment()"
          :class="equipmentMenuShowed ? 'isShowed' : ''"
        ).tabs-controls__show-equipment {{ tabName }}
        tabs(
          :tabs="equipmentTabs"
          :current-tab="currentTab"
          :wrapper-class="'container'"
          :tab-class="'tab-control'"
          :tab-active-class="'tab-control--active'"
          :line-class="'tab-control__active-line'"
          :class="equipmentMenuShowed ? 'isShowed' : ''"
          @onClick="handleClick"
        )
      .tab-content
        .container
          div(
            v-for="(item, index) in equipmentTabs"
            v-show="currentTab === item.value"
            :key='index'
          ).tab-content__wrapper
            .tab-content__picture
              img(:src="item.imageSrc" :alt="item.value")
            article.tab-content__description
              div(v-html="item.content")
              a(href="#" @click.prevent="showParametrs()").equipment__show-params
                | Характеристики
              transition(name="fade" tag="div" class="equipment__parametrs")
                div(v-if="isShowedParams" v-html="item.parametrs")
              footer(v-html="item.footer")
              //- strong.equipment-price(data-aos="fade")
                | Стоимость: уточняйте
                //- svg-icon(name="icon-ruble")
              app-btn(@click.native="showPopup()")
                | Заказать оборудование
            transition(name="fade")
              modal-window(@modalShow="setPopupVisibility()" v-if="isModalVisible" :formName="item.title")
</template>

<script>
import Tabs from 'vue-tabs-with-active-line'
import { mapGetters, mapActions } from 'vuex'
import { TweenMax, TimelineMax } from 'gsap'
import AppTabs from '~/components/AppUi/AppTabs'
import AppBtn from '~/components/AppUi/AppBtn'
import AppPopup from '~/components/AppUi/AppPopup'
import AppPreloader from '~/components/AppUi/AppPreloader'

const ModalWindow = () => ({
  component: import('~/components/AppUi/AppPopup.vue'),
  loading: AppPreloader,
  timeout: 3000
})

export default {
  name: 'Equipment',
  components: { AppPopup, AppBtn, AppTabs, ModalWindow, Tabs },
  data: () => ({
    isModalVisible: false,
    isShowedParams: false,
    equipmentMenuShowed: false,
    tabName: ''
    // tabs: equipmentTabs
  }),
  computed: {
    ...mapGetters({
      currentTab: 'ui/getCurrentTab',
      equipmentTabs: 'equipment/G_EQUIPMENT'
    })
  },
  created() {
    this.getEquipmentList()
  },
  mounted() {
    if (sessionStorage.vh && window.innerWidth <= 768) {
      const tabObj = JSON.parse(sessionStorage.vh)
      this.getCurrentTabName(tabObj.ui.currentTab)
    }
    this.animationEquipment()
  },
  methods: {
    ...mapActions({
      setCurrentTab: 'ui/setCurrentTab'
    }),
    ...mapActions({
      getEquipmentList: 'equipment/A_GET_EQUIPMENT_DATA'
    }),
    handleClick(newTab) {
      this.setCurrentTab(newTab)
      this.animationSwitch()
      if (window.innerWidth <= 768) {
        this.getCurrentTabName(newTab)
        this.showEquipment()
      }
    },
    getCurrentTabName(thisTab) {
      this.equipmentTabs.forEach((item) => {
        if (item.value === thisTab) {
          this.tabName = item.title
        }
      })
    },
    showPopup() {
      this.isModalVisible = true
    },
    setPopupVisibility(showed) {
      this.isModalVisible = showed
    },
    showParametrs() {
      this.isShowedParams = !this.isShowedParams
    },
    showEquipment() {
      this.equipmentMenuShowed = !this.equipmentMenuShowed
    },
    animationEquipment() {
      const timelineMachine = new TimelineMax()
      // const timelineTomato = new TimelineMax()

      // timelineTomato.add([
      //   TweenMax.to('.equipment__tomato--first', 0.4, {
      //     x: '110px'
      //   }),
      //   TweenMax.to('.equipment__tomato--second', 0.8, {
      //     x: '80px',
      //     y: '-55px'
      //   })
      // ])
      const timelineWheat = new TimelineMax()

      timelineWheat.add([
        TweenMax.to('.equipment__wheat-pic--1', 0.5, {
          x: '-10px',
          y: '-5px'
        }),
        TweenMax.to('.equipment__wheat-pic--2', 1, {
          x: '0',
          y: '-15px'
        }),
        TweenMax.to('.equipment__wheat-pic--3', 0.5, {
          x: '10px',
          y: '-15px'
        }),
        TweenMax.to('.equipment__wheat-pic--big', 1, {
          x: '15px',
          y: '-15px'
        })
      ])

      const wheatScene = this.$scrollmagic
        .scene({
          offset: '-80px',
          triggerElement: '.equipment',
          triggerHook: 0,
          duration: '100%'
        })
        .setTween(timelineWheat)

      timelineMachine.fromTo(
        '.tab-content__picture img',
        1,
        { scale: 0 },
        { scale: 1 }
      )

      // const tomatoScene = this.$scrollmagic
      //   .scene({
      //     triggerElement: '.equipment',
      //     triggerHook: 0,
      //     duration: '100%'
      //   })
      //   .setTween(timelineTomato)

      this.$scrollmagic.addScene([wheatScene])
    },
    animationSwitch() {
      const timelineMachine = new TimelineMax()
      const timelineContent = new TimelineMax()

      timelineMachine.fromTo(
        '.tab-content__picture img',
        0.4,
        { scale: 0 },
        { scale: 1 }
      )

      timelineContent.fromTo(
        '.tab-content__description',
        0.6,
        { opacity: 0 },
        { opacity: 1 }
      )
    }
  }
}
</script>

<style lang="scss">
.tabs-controls {
  position: relative;
  width: 100%;
  border-top: 1px solid #bdbdbd;
  border-bottom: 1px solid #bdbdbd;

  &::-webkit-scrollbar {
    display: none;
  }

  &__show-equipment.btn {
    padding: 18px 0;
    position: relative;
    cursor: pointer;
    outline: none;
    border: none;
    background-color: transparent;
    color: $color-primary;
    margin: 0 auto;
    width: 80%;
    max-width: 100%;

    &::before {
      @include triangle($color-primary, 10px, 'down');
      content: '';
      position: absolute;
      right: 0;
      top: 50%;
      margin-top: -5px;
      transition: transform 0.3s;
    }

    @include media($width_xs, 1) {
      display: none;
    }

    &.isShowed {
      color: $color-accent;

      &::before {
        transform: rotate(0);
        transition: transform 0.3s;
      }
    }
  }

  .container {
    display: flex;
    flex-flow: row nowrap;

    @include media($width_xs, 0) {
      //   width: fit-content;
      //   min-width: 1024px;
      //   overflow-x: scroll;
      flex-flow: column nowrap;
      align-items: center;
      opacity: 0;
      visibility: hidden;
      transition: opacity 0.5s, visibility 0.8s;

      &.isShowed {
        opacity: 1;
        visibility: visible;
        transition: opacity 0.5s;

        .tab-control {
          padding: 18px 0;
          font-size: 16px;
          transition: padding 0.4s;
        }
      }
    }
  }

  @include media($width_xs, 0) {
    text-align: center;
    // overflow-x: scroll;
    // overflow-y: hidden;
    // box-sizing: content-box;
    // scrollbar-width: none;
  }
}

.tab-control {
  padding: 10px 0;
  position: relative;
  cursor: pointer;
  outline: none;
  border: none;
  background-color: transparent;

  &__active-line {
    position: absolute;
    top: calc(100% - 1.5px);
    left: 0;
    height: 3px;
    background-color: $color-primary;
    border-radius: $brs;
    transition: transform 0.4s ease, width 0.4s ease;

    @include media($width_xs, 0) {
      display: none;
      bottom: 0;
    }
  }

  &:not(:last-of-type) {
    margin-right: 30px;

    @include media($width-container, 0) {
      margin-right: 24px / $width-container * 100vw;
    }

    @include media($width_xs, 0) {
      margin: 0;
    }
  }

  @include media($width-container, 0) {
    white-space: nowrap;
    font-size: 16px / $width-container * 100vw;
  }

  @include media($width_xs, 0) {
    width: auto;
    font-size: 0;
    padding: 0;
    transition: padding 0.4s, font-size 0.2s;
  }
}

.tab-control--active {
  position: relative;

  @include media($width_xs, 0) {
    display: none;
    // &::after {
    //   content: '';
    //   position: absolute;
    //   bottom: 0;
    //   left: 0;
    //   width: 100%;
    //   height: 2px;
    //   background-color: $color-primary;
    // }
  }
}

.tab-content {
  padding: 120px 0;

  &__wrapper {
    display: grid;
    gap: 60px;
    align-items: start;
    grid-template-columns: repeat(2, 1fr);

    @include media($width_xs, 0) {
      gap: 20px / $width_xs * 100vw;
      padding: 60px / $width_xs * 100vw 0;
      grid-template-columns: repeat(1, 1fr);
    }
  }

  &__picture {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 580px;

    &:after {
      content: '';
      position: absolute;
      left: 50%;
      top: 50%;
      width: 580px;
      height: 580px;
      border-width: 100px;
      border-radius: 50%;
      border-color: $color-circles;
      border-style: solid;
      transform: translate(-50%, -50%);

      @include media($width-container, 0) {
        width: 580px / $width-container * 100vw;
        height: 580px / $width-container * 100vw;
        border-width: 100px / $width-container * 100vw;
      }

      @include media($width_xs, 0) {
        width: 580px / $width_xs * 100vw;
        height: 580px / $width_xs * 100vw;
        border-width: 100px / $width_xs * 100vw;
      }
    }

    img {
      position: relative;
      z-index: 1;
      max-width: 100%;
      height: auto;
    }

    @include media($width-container, 0) {
      min-height: 580px / $width-container * 100vw;
    }

    @include media($width_xs, 0) {
      margin: auto;
      max-width: 580px / $width_xs * 100vw;
      min-height: 580px / $width_xs * 100vw;
    }
  }

  &__description {
    display: flex;
    flex-flow: column nowrap;

    > .btn {
      @include media($width_xs_s, 0) {
        max-width: 100%;
      }
    }
  }
  @include media($width_xs, 0) {
    padding: 60px / $width_xs * 100vw 0;
  }
}
.equipment {
  position: relative;
  padding-top: 0;

  &__wheat {
    position: absolute;
    top: 10%;
    right: 0;
    height: calc(320px / 1.5);
    width: calc(200px / 1.5);

    &-pic {
      position: absolute;
      right: 0;
      top: 0;

      &--1 {
        right: calc(74px / 1.5);
        top: calc(3px / 1.5);
        width: calc(21px / 1.5);
        height: calc(24px / 1.5);

        @include media($width-container, 0) {
          right: (74px / 1.5) / $width-container * 100vw;
          top: (3px / 1.5) / $width-container * 100vw;
          width: (21px / 1.5) / $width-container * 100vw;
          height: (24px / 1.5) / $width-container * 100vw;
        }

        @include media($width_xs, 0) {
          right: (74px / 1.3) / $width_xs * 100vw;
          top: (3px / 1.3) / $width_xs * 100vw;
          width: (21px / 1.3) / $width_xs * 100vw;
          height: (24px / 1.3) / $width_xs * 100vw;
        }
      }

      &--2 {
        right: calc(43px / 1.5);
        top: calc(-29px / 1.5);
        width: calc(14px / 1.5);
        height: calc(22px / 1.5);

        @include media($width-container, 0) {
          right: (43px / 1.5) / $width-container * 100vw;
          top: (-29px / 1.5) / $width-container * 100vw;
          width: (14px / 1.5) / $width-container * 100vw;
          height: (22px / 1.5) / $width-container * 100vw;
        }

        @include media($width_xs, 0) {
          right: (43px / 1.3) / $width_xs * 100vw;
          top: (-29px / 1.3) / $width_xs * 100vw;
          width: (14px / 1.3) / $width_xs * 100vw;
          height: (22px / 1.3) / $width_xs * 100vw;
        }
      }

      &--3 {
        top: calc(8px / 1.5);
        right: calc(18px / 1.5);
        width: calc(24px / 1.5);
        height: calc(29px / 1.5);

        @include media($width-container, 0) {
          top: (8px / 1.3) / $width-container * 100vw;
          right: (18px / 1.3) / $width-container * 100vw;
          width: (24px / 1.3) / $width-container * 100vw;
          height: (29px / 1.3) / $width-container * 100vw;
        }

        @include media($width_xs, 0) {
          top: (8px / 1.3) / $width_xs * 100vw;
          right: (18px / 1.3) / $width_xs * 100vw;
          width: (24px / 1.3) / $width_xs * 100vw;
          height: (29px / 1.3) / $width_xs * 100vw;
        }
      }
      &--big {
        right: calc(-240px / 1.5);
        height: calc(288px / 1.5);
        width: calc(444px / 1.5);

        @include media($width-container, 0) {
          right: (-240px / 1.5) / $width-container * 100vw;
          height: (288px / 1.5) / $width-container * 100vw;
          width: (444px / 1.5) / $width-container * 100vw;
        }

        @include media($width_xs, 0) {
          right: (-240px / 1.3) / $width_xs * 100vw;
          height: (288px / 1.3) / $width_xs * 100vw;
          width: (444px / 1.3) / $width_xs * 100vw;
        }
      }
    }

    @include media($width-container, 0) {
      height: (320px / 1.5) / $width-container * 100vw;
      width: (200px / 1.5) / $width-container * 100vw;
    }

    @include media($width_xs, 0) {
      top: auto;
      bottom: 0;
      height: (320px / 1.3) / $width_xs * 100vw;
      width: (200px / 1.3) / $width_xs * 100vw;
    }

    @include media($width_xs_s, 0) {
      display: none;
    }
  }

  // &__tomato {
  //   position: absolute;

  //   img {
  //     width: 100%;
  //     height: 100%;
  //   }

  //   &--first {
  //     right: 170px;
  //     top: 100px;
  //     width: 90px;
  //     height: 97px;

  //     @include media($width-container, 0) {
  //       right: 170px / $width-container * 100vw;
  //       top: 100px / $width-container * 100vw;
  //       width: 90px / $width-container * 100vw;
  //       height: 97px / $width-container * 100vw;
  //     }
  //   }

  //   &--second {
  //     right: 40px;
  //     top: 180px;
  //     width: 59px;
  //     height: 62px;

  //     @include media($width-container, 0) {
  //       right: 40px / $width-container * 100vw;
  //       top: 180px / $width-container * 100vw;
  //       width: 59px / $width-container * 100vw;
  //       height: 62px / $width-container * 100vw;
  //     }
  //   }

  //   @include media($width_xs, 0) {
  //     display: none;
  //   }
  // }

  &__list {
    list-style: none;
    padding-left: 0;
    margin-bottom: 22px;
  }

  &__show-params {
    margin-bottom: 22px;
  }

  &__housing {
    margin-bottom: 22px;

    &--selected {
      .vs__dropdown-toggle {
        &::before {
          left: 0;
          width: 100%;
          background-color: $color-accent;
          transition: left 0.4s, width 0.4s, background-color 0.1s;
        }
      }
    }
  }

  &-price {
    margin-bottom: 40px;

    svg {
      margin-left: 3px;
      width: 12px;
      height: 12px;
    }
  }

  @include media($width_xs, 0) {
    padding-bottom: 60px;
  }

  @include media($width_xs_s, 0) {
    padding-bottom: 60px;
  }
}
</style>
