FROM node:14-alpine

RUN mkdir -p /root/app/

WORKDIR /root/app/

COPY package*.json /root/app/
COPY . /root/app/

RUN npm install --progress=false

ENV NODE_ENV=production

RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000

CMD ["npm", "start"]
