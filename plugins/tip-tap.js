import Vue from 'vue'
import wysiwyg from 'vue-wysiwyg'
import 'vue-wysiwyg/dist/vueWysiwyg.css'
Vue.use(wysiwyg, { locale: 'ru' })

// import { TiptapVuetifyPlugin } from 'tiptap-vuetify'

// export default ({ app }) => {
//   Vue.use(TiptapVuetifyPlugin, { vuetify: app.vuetify, iconsGroup: 'mdi' })
// }
