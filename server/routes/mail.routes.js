const { Router } = require('express')
const {
  mailPasswordRec,
  mailSignUp
} = require('../controllers/mail.controller')
const router = Router()

router.post('/recovery', mailPasswordRec)
router.post('/sign-up', mailSignUp)

module.exports = router
