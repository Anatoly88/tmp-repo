const passport = require('passport')
const { Router } = require('express')
const upload = require('../middleware/upload')
const controller = require('../controllers/equipments.controller')
const router = Router()

// Admin router /api/equipments-admin/admin
router.post(
  '/admin/',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  controller.createEquipment
)

router.get(
  '/admin/',
  passport.authenticate('jwt', { session: false }),
  controller.getAllEquipments
)

router.get(
  '/admin/:name',
  passport.authenticate('jwt', { session: false }),
  controller.getEquipmentByName
)

router.put(
  '/admin/:name',
  passport.authenticate('jwt', { session: false }),
  controller.updateEquipment
)

router.delete(
  '/admin/:name',
  passport.authenticate('jwt', { session: false }),
  controller.deleteEquipment
)

// Base routes /api/equipments-admin/
// Подумать как выводить контент на роуты динамично

// router.get('/equipments', controller.getAll)
// //
// router.get('equipments/:id', controller.getPage)

router.get('/', controller.getAllEquipments)
router.get('/:name', controller.getEquipmentByName)

module.exports = router
