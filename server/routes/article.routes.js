const { Router } = require('express')
// const passport = require('passport')
const {
  createArticle,
  getAll,
  getArticle,
  updateArticle
} = require('../controllers/article.controller')
const router = Router()

//  GET /api/article/list
router.get('/list', getAll)

//  GET /api/article/:id
router.get('/:id', getArticle)

//  POST /api/article/create
router.post('/create', createArticle)

//  PUT /api/article/update
router.put('/update/:slug', updateArticle)

module.exports = router
