const { Router } = require('express')
const { imageFileName } = require('../controllers/upload.controller')
const upload = require('../middleware/upload')
const router = Router()

// POST /api/upload/image
router.post('/image', upload.single('image'), imageFileName)

module.exports = router
