const passport = require('passport')
const { Router } = require('express')
const upload = require('../middleware/upload')
const ctr = require('../controllers/post.controller')
const router = Router()

// Admin
// /api/post/admin
router.post(
  '/admin/',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  ctr.createPost
)

router.get(
  '/admin/',
  passport.authenticate('jwt', { session: false }),
  ctr.getPostsAll
)

router.get(
  '/admin/:id',
  passport.authenticate('jwt', { session: false }),
  ctr.getPostById
)

router.put(
  '/admin/:id',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  ctr.updatePost
)

router.delete(
  '/admin/:id',
  passport.authenticate('jwt', { session: false }),
  ctr.removePost
)

// Base
// /api/post
router.get('/', ctr.getPostsAll)
router.get('/:id', ctr.getPostById)

module.exports = router
