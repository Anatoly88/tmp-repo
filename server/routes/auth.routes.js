const { Router } = require('express')

const {
  login,
  createUser,
  updateUser
} = require('../controllers/auth.contoroller')
const router = Router()

// path /api/auth/admin/login
router.post('/admin/login', login)

// path /api/auth/create
router.post('/create', createUser)

// path /api/auth/update
router.put('/update', updateUser)

module.exports = router
