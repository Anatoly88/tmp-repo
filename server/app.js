const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const passport = require('passport')

const passportStrategy = require('./middleware/passport-strategy')

const authRoutes = require('./routes/auth.routes')
const euipmentsRoutes = require('./routes/equipments.routes')
const postRoutes = require('./routes/post.routes')
const downloadRoutes = require('./routes/download.routes')
const userRoutes = require('./routes/user.routes')
const mailRoutes = require('./routes/mail.routes')
const articleRoutes = require('./routes/article.routes')
const uploadRoutes = require('./routes/upload.routes')

const keys = require('./keys')

mongoose
  .connect(keys.MONGO_URI, {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: true
  })
  .then(() => {
    console.log('mongo connected')
  })
  .catch((error) => console.log('mongo error', error))

app.use(passport.initialize())
passport.use(passportStrategy)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api/auth', authRoutes)
app.use('/api/equipments-admin', euipmentsRoutes)
app.use('/api/post', postRoutes)
app.use('/api/article', articleRoutes)
app.use('/api/download', downloadRoutes)
app.use('/api/user', userRoutes)
app.use('/api/mail', mailRoutes)
app.use('/api/upload', uploadRoutes)

module.exports = app
