module.exports.imageFileName = (req, res) => {
  try {
    const imageUrl = { imageUrl: req.file.filename }
    res.status(200).json(imageUrl)
  } catch (e) {
    res.status(500).json(e)
  }
}
