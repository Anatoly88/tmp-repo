const Post = require('../models/post.model')

module.exports.createPost = async (req, res) => {
  const post = new Post({
    title: req.body.title,
    // imageUrl: `/${req.file.filename}`,
    content: req.body.content,
    preview: req.body.preview,
    date: req.body.date,
    videoUrl: req.body.videoUrl
  })

  try {
    await post.save()
    res.status(201).json(post)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getPostsAll = async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 })
    res.json(posts)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getPostById = async (req, res) => {
  try {
    await Post.findById(req.params.id)
      .populate('comments')
      .exec((_error, post) => {
        res.json(post)
      })
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.updatePost = async (req, res) => {
  // const $set = {
  //   title: req.body.title,
  //   content: req.body.content,
  //   imageUrl: `/${req.file.filename}`,
  //   preview: req.body.preview,
  //   date: req.body.date,
  //   videoUrl: req.body.videoUrl
  // }
  try {
    const post = await Post.findOneAndUpdate(
      {
        _id: req.params.id
      },
      { $set: req.body },
      { new: true }
    )
    res.json(post)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.removePost = async (req, res) => {
  try {
    await Post.deleteOne({ _id: req.params.id })
    res.json({ message: 'Пост удален' })
  } catch (e) {
    res.status(500).json(e)
  }
}
