const { Schema, model } = require('mongoose')

const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: false
  },
  content: {
    type: String,
    required: true
  },
  preview: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  videoUrl: {
    type: String,
    required: false
  }
})

module.exports = model('posts', postSchema)
