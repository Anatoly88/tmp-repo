// eslint-disable-next-line nuxt/no-cjs-in-config
module.exports = {
  telemetry: false,
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Растворные узлы NAGRO для производства КАС 32 и ЖКУ',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      {
        name: 'yandex-verification',
        content: 'ff0eabd9a266dd85'
      },
      {
        name: 'yandex-verification',
        content: '3df2a86cefd2048e'
      }
    ],
    script:
      process.env.NODE_ENV === 'production'
        ? [
            {
              src: 'https://code.jquery.com/jquery-2.2.4.min.js',
              body: false,
              integrity: 'sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=',
              crossorigin: 'anonymous'
            },
            {
              src: '/ym.js',
              body: false,
              type: 'text/javascript'
            }
          ]
        : [],
    noscript:
      process.env.NODE_ENV === 'production'
        ? [
            {
              innerHTML: `<div><img src="https://mc.yandex.ru/watch/45578973" style="position:absolute; left:-9999px;" alt="" /></div>`
            },
            {
              innerHTML: `<div><img
        src="https://vk.com/rtrg?p=VK-RTRG-551313-cA8y0"
        style="position:fixed; left:-999px;"
        alt=""
      /></div>`
            },
            {
              innerHTML: `<div><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=706676300098615&ev=PageView&noscript=1" /></div>`
            }
          ]
        : [],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ],
    __dangerouslyDisableSanitizers: ['script', 'noscript']
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#1abb4f'
  },
  /*
   ** Global CSS
   */
  css: [
    'normalize.css/normalize.css',
    'izitoast/dist/css/iziToast.css',
    'aos/dist/aos.css',
    'vue-glide-js/dist/vue-glide.css',
    'vue-select/dist/vue-select.css',
    'video.js/dist/video-js.css',
    './assets/scss/main.scss',
    'element-ui/lib/theme-chalk/upload.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/vuex-persist',
      ssr: false
    },
    {
      src: '~/plugins/vue-scrollmagic.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-aos.js',
      ssr: false
    },
    {
      src: '~/plugins/glide.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-image-lightbox.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-izitoast.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-textarea-autoresize.js',
      ssr: false
    },
    {
      src: '~/plugins/polyfills',
      mode: 'client'
    },
    '~/plugins/validate.js',
    '~/plugins/axios.js',
    {
      src: '~/plugins/vue-mask.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-markdown.js',
      ssr: false
    },
    {
      src: '~/plugins/vue-file-upload.js',
      ssr: false
    },
    '~/plugins/youtube',
    '~plugins/element-ui.js',
    '~plugins/vue-js-modal.js',
    { src: '~/plugins/tip-tap.js', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/style-resources',
    '@nuxtjs/svg-sprite',
    'vue-yandex-maps/nuxt',
    'cookie-universal-nuxt',
    'nuxt-device-detect'
    // [
    //   '@nuxtjs/google-gtag',
    //   {
    //     id: 'AW-790085580',
    //     additionalAccounts: [
    //       {
    //         id: 'UA-150310810-1'
    //       }
    //     ]
    //   }
    // ]
  ],
  styleResources: {
    scss: [
      './assets/scss/_fonts.scss',
      './assets/scss/_var.scss',
      './assets/scss/_mixins.scss'
    ]
  },
  serverMiddleware: ['~/api/contact'],
  svgSprite: {
    input: '~/static/images/svg/src',
    output: '~/static/images/svg/gen'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL:
      process.env.NODE_ENV === 'production'
        ? 'https://www.nagro.group'
        : 'http://localhost:3000',
    proxy: process.env.NODE_ENV === 'production',
    https: process.env.NODE_ENV === 'production'
  },
  // proxy: {
  //   '/api': 'https://www.nagro.group'
  // },
  //
  //
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    babel: {
      presets({ isServer }) {
        const targets = isServer ? { node: 'current' } : { ie: 11 }
        return [[require.resolve('@nuxt/babel-preset-app'), { targets }]]
      }
    },
    transpile: ['vee-validate/dist/rules', 'vue-youtube-embed'],
    vendor: ['vue-aos'],
    extend(config, ctx) {}
  }
}
