export const callbackPhone = '8 (800) 707 98 31'
export const callbackPhoneUtm = '8 (800) 707 96 81'
export const SET_CURRENT_TAB = 'UI_SET_CURRENT_TAB'
export const GET_ADMIN_NAV_LIST = [
  {
    name: 'Добавить оборудование',
    path: '/admin/create-equipment'
  },
  {
    name: 'Оборудование',
    path: '/admin/equipments-admin'
  },
  {
    name: 'Добавить новость',
    path: '/admin/create-new'
  },
  {
    name: 'Новости',
    path: '/admin/news'
  },
  {
    name: 'Добавить статью',
    path: '/admin/create-article'
  },
  {
    name: 'Статьи',
    path: '/admin/articles'
  },
  {
    name: 'Пользователи',
    path: '/admin/users'
  },
  {
    name: 'Выйти',
    path: '/admin/logout'
  }
]
