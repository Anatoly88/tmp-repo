import { SET_CURRENT_TAB } from '~/store/constants'

export const state = () => ({
  currentTab: 'RUKAS_150'
})

export const getters = {
  getCurrentTab: (state) => {
    return state.currentTab
  }
}

export const mutations = {
  [SET_CURRENT_TAB](state, data) {
    state.currentTab = data
  }
}

export const actions = {
  setCurrentTab({ commit }, payload) {
    commit(SET_CURRENT_TAB, payload)
  }
}
