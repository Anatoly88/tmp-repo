export const actions = {
  async A_UPLOAD_AVATAR({ commit }, image) {
    try {
      const fd = new FormData()
      fd.append('image', image, image.name)
      return await this.$axios.post('/api/user/upload', fd)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  }
}
