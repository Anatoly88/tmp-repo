import { GET_ADMIN_NAV_LIST } from '~/store/constants'

export const state = () => ({
  S_ADMIN_NAV_LIST: []
})

export const mutations = {
  M_ADMIN_NAV_LIST(state, adminNavList) {
    state.S_ADMIN_NAV_LIST = adminNavList
  }
}

export const actions = {
  A_ADMIN_NAV_LIST({ commit }) {
    commit('M_ADMIN_NAV_LIST', GET_ADMIN_NAV_LIST)
  }
}

export const getters = {
  G_ADMIN_NAV_LIST: (s) => s.S_ADMIN_NAV_LIST
}
