export const state = () => ({
  S_POSTS: []
})

export const mutations = {
  M_POSTS(state, posts) {
    state.S_POSTS = posts
  }
}

export const actions = {
  async fetchPostsAdmin({ commit }) {
    try {
      return await this.$axios.$get('/api/post/admin')
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchPosts({ commit }) {
    try {
      return await this.$axios.$get('/api/post')
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async removePost({ commit }, id) {
    try {
      return await this.$axios.$delete(`/api/post/admin/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async updatePost(
    { commit },
    { id, title, content, image, preview, date, videoUrl }
  ) {
    try {
      const fData = new FormData()
      fData.append('title', title)
      fData.append('content', content)
      fData.append('image', image, image.name)
      fData.append('preview', preview)
      fData.append('date', date)
      fData.append('videoUrl', videoUrl)

      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }

      config.headers['X-HTTP-Method-Override'] = 'PUT'

      return await this.$axios.$post(`/api/post/admin/${id}`, fData, config)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async createPost(
    { commit },
    { title, content, image, preview, date, videoUrl }
  ) {
    try {
      const fd = new FormData()

      fd.append('title', title)
      fd.append('content', content)
      fd.append('preview', preview)
      fd.append('date', date)
      fd.append('videoUrl', videoUrl)
      // fd.append('image', image, image.name)

      return await this.$axios.$post('/api/post/admin', fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchPostAdminById({ commit }, id) {
    try {
      return await this.$axios.$get(`/api/post/admin/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchPostById({ commit }, id) {
    try {
      return await this.$axios.$get(`/api/post/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async addView({ commit }, { views, _id }) {
    try {
      return await this.$axios.$put(`/api/post/add/view/${_id}`, { views })
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async getAnalytics({ commit }) {
    try {
      return await this.$axios.$get('/api/post/admin/get/analytics')
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }
}

export const getters = {
  G_POSTS: (s) => s.S_POSTS
}
